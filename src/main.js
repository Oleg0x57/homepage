import Vue from 'vue'
import Home from './pages/Home.vue'
import About from './pages/About.vue'
import NotFound from './pages/NotFound.vue'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'

Vue.config.productionTip = false
Vue.use(VueMaterial);

const routes = {
    '/': Home,
    '/about': About
};

new Vue({
    el: '#app',
    data:{
        currentRoute: window.location.pathname
    },
    computed: {
        currentPage(){
            return routes[this.currentRoute] || NotFound;
        }
    },
    render (h) { return h(this.currentPage) }
});
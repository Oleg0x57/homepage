import { shallowMount } from '@vue/test-utils'
import NotFound from '@/pages/NotFound.vue'

describe('NotFound.vue', () => {
  it('renders 404 content', () => {
    const wrapper = shallowMount(NotFound)
    expect(wrapper.text()).toMatch('404')
  })
})

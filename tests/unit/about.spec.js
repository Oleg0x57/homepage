import { shallowMount } from '@vue/test-utils'
import About from '@/pages/About.vue'

describe('About.vue', () => {
  it('renders About page content', () => {
    const wrapper = shallowMount(About)
    expect(wrapper.text()).toMatch('About page')
  })
})

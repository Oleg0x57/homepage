import { shallowMount } from '@vue/test-utils'
import Home from '@/pages/Home.vue'

describe('Home.vue', () => {
  it('renders Home page content', () => {
    const wrapper = shallowMount(Home)
    expect(wrapper.text()).toMatch('Home page')
  })
})
